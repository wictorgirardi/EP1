Quanto a execução do programa: 
-Abertura de arquivo, interface de usuario, descriptação de imagens PPM e PGM funcionando.

Quanto ao código: 
-Todas as partes realizadas com os conceitos pedidos e usando de maneira ativa o gitlab como repositório remoto.

Para a execução: 
- Faça download do repositório remoto do gitlab ou use git clone.
- no destinatario do arquivo de make clean e em seguida make para compilação.
- Para a execução, make run.	
- Com a interface do programa aberta selecione entre 1 ou 2, sendo 1 a alternativa usada para abertura de imagem e desencriptação e a opção 2 sendo a saída do programa.
