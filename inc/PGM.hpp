
#include <string>
#include "Foto.hpp"
#ifndef PGM_HPP
#define PGM_HPP

using namespace std;

class PGM: public Foto {
private: PGM();
public:
    PGM(string Arquivo);
    ~PGM();
    void mensagem();
};

#endif
