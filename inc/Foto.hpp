#ifndef Foto_hpp
#define Foto_hpp
#include <string>

using namespace std;

class Foto {
private:
    //Declaração de atributos//
    string Arquivo;
    string tipo;
    char comentario;
    string cyphr;
    int alt, intensidade, lar, posicao, tamanho, inic;
    Foto();
protected:
    char *pixel;
public:
    //Modelo de métodos//
    Foto(string Arquivo);
    ~Foto();
    
    int getInic();
    int getTamanho();
    string getCyphr();
    char getPixel();
    void mensagem();
};
#endif

