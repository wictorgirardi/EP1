#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "PGM.hpp"

using namespace std;

PGM::PGM(string Arquivo):Foto(Arquivo){}
PGM::~PGM(){}
void PGM::mensagem(){
    int posicao = 0;
    int cyphr = 0;
    string resposta = "";
    stringstream(getCyphr()) >> cyphr;
    for (posicao = getInic(); posicao < (getInic() + getTamanho()); posicao++) {
        if (pixel[posicao] < 32) {
            pixel[posicao] = 32;
        }
        if ((pixel[posicao] > 64 && pixel[posicao] < 91) || (pixel[posicao] > 96 && pixel[posicao] < 123)) {
            if ((pixel[posicao] < (97 + cyphr) && pixel[posicao] > 96) || (pixel[posicao] < (65 + cyphr) && pixel[posicao] > 64)) {
                pixel[posicao] = pixel[posicao] + 26;
            }
            pixel[posicao] = pixel[posicao] - cyphr;
        }
        resposta = resposta + pixel[posicao];
    }
    cout <<"A mensagem decifrada e: "<< resposta << endl;
}
