#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <bits/stdc++.h>
#include "Foto.hpp"



Foto::Foto(string Arquivo) {
    cout << endl << endl << "Local informado: "<< Arquivo << endl;
    ifstream exemplo;
    exemplo.open(Arquivo, ios::binary);
    if(exemplo.is_open()){
//Realizando alocação de memoria//
        getline(exemplo, tipo);
        exemplo >> comentario;
        exemplo >> inic;
        exemplo >> tamanho;
        exemplo >> cyphr;
        exemplo >> lar;
        exemplo >> alt;
        exemplo >> intensidade;
//Iniciando a contagem do numero de caracteres inteiros//
        int tam = 10;
        int limite;
        for(limite = 1; inic >= limite; tam++) {
            limite *= 10;
        }
        for(limite = 1; tamanho >= limite; tam++) {
            limite *= 10;
        }
        for(limite = 1; lar >= limite; tam++) {
            limite *= 10;
        }
        for(limite = 1; alt>= limite; tam++) {
            limite *= 10;
        }
        for(limite = 1; intensidade >= limite; tam++) {
            limite *= 10;
        }
        tam += cyphr.size();
        exemplo.seekg(tam, ios::end);
        posicao = exemplo.tellg();
        exemplo.seekg(tam, ios::beg);
        pixel = new char [posicao];
        exemplo.read(pixel,posicao);
        exemplo.close();
    }
    else cout << "Erro ao abrir a imagem" << endl;
}

//Destrutor da classe imagem//
Foto::~Foto(){}

int Foto::getInic(){
    return inic;
}
int Foto::getTamanho(){
    return tamanho;
}
string Foto::getCyphr(){
    return cyphr;
}
char Foto::getPixel(){
    return * pixel;
}
//Fazendo a descodificação da mensagem oculta//
void Foto::mensagem() {
    // Uso do unsigned char para melhor restriçao de valores//
    unsigned char inteiro[3*tamanho];
    int i, j;
    string msg_cod = "";
    
    for (i = 0, posicao = inic; posicao < (inic + 3*tamanho); ++posicao, ++i) {
        inteiro[i] = (unsigned char)pixel[posicao] % 10;
    }
    for (i = 0, j = 0; i < 3*tamanho; i += 3, j++) {
        unsigned char rgb = inteiro[i] + inteiro[i+1] + inteiro[i+2] + 64;
        if (rgb == 64) {
            rgb = 32;
        }
        msg_cod += (char)rgb;
    }
    // gerando o alfabeto cifrado
    string alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    string alfabeto_cifrado = "";
    bool posicao_letras[26] = {0};
    int tamanho_chave = cyphr.size();
    for (i = 0; i < tamanho_chave; i++) {
        if (cyphr[i] >= 'A' && cyphr[i] <= 'Z') {
            if (posicao_letras[cyphr[i] - 65] == 0) {
                alfabeto_cifrado += cyphr[i];
                posicao_letras[cyphr[i] - 65] = 1;
            }
        }
        else if (cyphr[i] >= 'a' && cyphr[i] <= 'z') {
            if (posicao_letras[cyphr[i] - 97] == 0) {
                alfabeto_cifrado += cyphr[i] - 32;
                posicao_letras[cyphr[i] - 97] = 1;
            }
        }
    }
    for (i = 0; i < 26; i++) {
        if (posicao_letras[i] == 0) {
            posicao_letras[i] = 1;
            alfabeto_cifrado += char(i + 65);
        }
    }
    map <char,int> alf_cif;
    for (i = 0; i < 26; i++) {
        alf_cif[alfabeto_cifrado[i]] = i;
    }
    string decifrada = "";
    int tamanho_mensagem = msg_cod.size();
    // decodificando a mensagem
    for (i = 0; i < tamanho_mensagem; i++) {
        if (msg_cod[i] >= 'a' && msg_cod[i] <= 'z') {
            int pos = alf_cif[msg_cod[i] - 32];
            decifrada += alfabeto[pos];
        }
        else if (msg_cod[i] >= 'A' && msg_cod[i] <= 'Z') {
            int pos = alf_cif[msg_cod[i]];
            decifrada += alfabeto[pos];
        }
        else {
            decifrada += msg_cod[i];
        }
    }
    cout << "Mensagem: " << decifrada << endl << endl;
    delete[] pixel;
}


