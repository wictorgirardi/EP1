#include <iostream>
#include <string>
#include <fstream>
#include "interface.hpp"
#include "Foto.hpp"
#include "PGM.hpp"

using namespace std;

void Menu(){
    cout <<"Bem vindo ao programa de leitura e desencriptação de arquivos PPM e PGM!"<< "\n" << endl;
    cout << "Dentro desse programa voce possui as seguintes opcoes\n*\n*\n*\n*\n*" << endl;
    cout << "**************************************************\n" << endl;
    cout << "* OPCAO 1 - DECIFRAR MENSAGENS PPM OU PGM........*\n" << endl;
    cout << "* OPCAO 2 - SAIR.................................*\n" << endl;
    cout << "**************************************************" << endl;
    cout << "*\n*\n*\n*\n*" << endl;
    cout << "Por favor, digite 1 ou 2 para fazer sua escolha:..\n" << endl;
}

void escolha1(){
    cout << "Muito obrigado por usar o meu primeiro programa em C++\n" << endl;
    cout << "Volte sempre!!........................................\n" << endl;
    cout << "Desenvolvedor: WICTOR BASTOS GIRARDI..................\n" << endl;
    cout << "Matricula: 1700/47326.................................\n" << endl;
}
